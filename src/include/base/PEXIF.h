/*
 * Copyright (C) 2016, Nils Asmussen <nils@os.inf.tu-dresden.de>
 * Economic rights: Technische Universitaet Dresden (Germany)
 *
 * This file is part of M3 (Microkernel-based SysteM for Heterogeneous Manycores).
 *
 * M3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * M3 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License version 2 for more details.
 */

#pragma once

#include <base/Common.h>

namespace m3 {

enum Operation : word_t {
    SLEEP,
    EXIT,
    YIELD,
    TRANSL_FAULT,
    FLUSH_INV,
    NOOP,
};

}

#if defined(__x86_64__)
#   include "arch/x86_64/PEXABI.h"
#elif defined(__arm__)
#   include "arch/arm/PEXABI.h"
#elif defined(__riscv)
#   include "arch/riscv/PEXABI.h"
#else
#   error "Unsupported ISA"
#endif

namespace m3 {

struct PEXIF {
    static void sleep(uint64_t nanos, epid_t ep = TCU::INVALID_EP) {
        PEXABI::call2(Operation::SLEEP, nanos, ep);
    }

    static void exit(int code) {
        PEXABI::call1(Operation::EXIT, static_cast<word_t>(code));
    }

    static void flush_invalidate() {
        PEXABI::call2(Operation::FLUSH_INV, 0, 0);
    }
};

}
