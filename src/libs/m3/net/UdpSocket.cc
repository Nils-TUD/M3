/*
 * Copyright (C) 2018, Nils Asmussen <nils@os.inf.tu-dresden.de>
 * Copyright (C) 2021, Tendsin Mende <tendsin.mende@mailbox.tu-dresden.de>
 * Copyright (C) 2017, Georg Kotheimer <georg.kotheimer@mailbox.tu-dresden.de>
 * Economic rights: Technische Universitaet Dresden (Germany)
 *
 * This file is part of M3 (Microkernel-based SysteM for Heterogeneous Manycores).
 *
 * M3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * M3 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License version 2 for more details.
 */

#include <m3/Exception.h>
#include <m3/net/Socket.h>
#include <m3/net/UdpSocket.h>
#include <m3/session/NetworkManager.h>

namespace m3 {

UdpSocket::UdpSocket(int sd, capsel_t caps, NetworkManager &nm)
    : Socket(sd, caps, nm) {
}

UdpSocket::~UdpSocket() {
    try {
        // we have no connection to tear down here, but only want to make sure that all packets we sent
        // are seen and handled by the server. thus, wait until we have got all replies to our
        // potentially in-flight packets, in which case we also have received our credits back.
        while(!_channel.has_all_credits())
            wait_for_credits();
    }
    catch(...) {
        // ignore errors
    }
}

Reference<UdpSocket> UdpSocket::create(NetworkManager &nm, const DgramSocketArgs &args) {
    capsel_t caps;
    int sd = nm.create(SocketType::DGRAM, 0, args, &caps);
    auto sock = new UdpSocket(sd, caps, nm);
    nm.add_socket(sock);
    return Reference<UdpSocket>(sock);
}

void UdpSocket::bind(port_t port) {
    if(_state != Closed)
        throw Exception(Errors::INV_STATE);

    IpAddr addr = _nm.bind(sd(), port);
    _local_ep.addr = addr;
    _local_ep.port = port;
    _state = State::Bound;
}

ssize_t UdpSocket::recv_from(void *dst, size_t amount, Endpoint *src_ep) {
    return Socket::do_recv(dst, amount, src_ep);
}

ssize_t UdpSocket::send_to(const void *src, size_t amount, const Endpoint &dst_ep) {
    return Socket::do_send(src, amount, dst_ep);
}

}
