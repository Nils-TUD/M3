/*
 * Copyright (C) 2018, Nils Asmussen <nils@os.inf.tu-dresden.de>
 * Economic rights: Technische Universitaet Dresden (Germany)
 *
 * This file is part of M3 (Microkernel-based SysteM for Heterogeneous Manycores).
 *
 * M3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * M3 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License version 2 for more details.
 */

use crate::time;

#[macro_export]
macro_rules! read_csr {
    ($reg_name:tt) => {{
        let res: usize;
        unsafe { llvm_asm!(concat!("csrr $0, ", $reg_name) : "=r"(res)) };
        res
    }}
}

#[macro_export]
macro_rules! write_csr {
    ($reg_name:tt, $val:expr) => {{
        unsafe { llvm_asm!(concat!("csrw ", $reg_name, ", $0") : : "r"($val) : : "volatile") };
    }};
}

#[macro_export]
macro_rules! set_csr_bits {
    ($reg_name:tt, $bits:expr) => {{
        unsafe { llvm_asm!(concat!("csrs ", $reg_name, ", $0") : : "r"($bits) : : "volatile") };
    }};
}

#[allow(clippy::missing_safety_doc)]
pub unsafe fn read8b(addr: usize) -> u64 {
    let res: u64;
    llvm_asm!(
        "ld $0, ($1)"
        : "=r"(res)
        : "r"(addr)
        : : "volatile"
    );
    res
}

#[allow(clippy::missing_safety_doc)]
pub unsafe fn write8b(addr: usize, val: u64) {
    llvm_asm!(
        "sd $0, ($1)"
        : : "r"(val), "r"(addr)
    )
}

#[inline(always)]
pub fn stack_pointer() -> usize {
    let sp: usize;
    unsafe {
        llvm_asm!(
            "mv $0, sp"
            : "=r"(sp)
        )
    }
    sp
}

#[inline(always)]
pub fn base_pointer() -> usize {
    let fp: usize;
    unsafe {
        llvm_asm!(
            "mv $0, fp"
            : "=r"(fp)
        )
    }
    fp
}

pub fn elapsed_cycles() -> time::Time {
    let mut res: time::Time;
    unsafe {
        llvm_asm!(
            "rdcycle $0"
            : "=r"(res)
            : : : "volatile"
        );
    }
    res
}

#[allow(clippy::missing_safety_doc)]
pub unsafe fn backtrace_step(bp: usize, func: &mut usize) -> usize {
    let bp_ptr = bp as *const usize;
    *func = *bp_ptr.offset(-1);
    *bp_ptr.offset(-2)
}

pub fn gem5_debug(msg: usize) -> time::Time {
    let mut res = msg as time::Time;
    unsafe {
        llvm_asm!(
            ".long 0xC600007B"
            : "+{x10}"(res)
            : : : "volatile"
        );
    }
    res
}
